﻿namespace P02.EraseMiddleElement
{
    public class Program
    {
        static void Main(string[] args)
        {
            //LinkedList<int> list = new LinkedList<int>();
            //list.AddFirst(1);
            //list.AddLast(2);
            //list.AddLast(3);
            //list.AddLast(4);
            //int size = list.Count;

            //int removed = list.ElementAt(size / 2);
            //Console.WriteLine(removed);

            //list.Remove(removed);
            //Console.WriteLine(string.Join(" ", list));
        }
    }

    public class LinkedList
    {
        public static LinkedList<int> EraseMiddle(LinkedList<int> elements)
        {
            // check for imput parameters. elements could be null or empty, or with 1 or 2 elements.
            int size = elements.Count;
            int removed = elements.ElementAt(size / 2);// removed gives that idea that it was removed but it is not at this point. rename to forRemoving for example
            elements.Remove(removed);

            // do not return the list. Returning list gives the idea that it is a copy.
            // you can return true or false depending on whether someting was deleted.
            return elements;
        }
    }
}