using NUnit.Framework;
using static P02.EraseMiddleElement.LinkedList;

namespace P02.EraseMiddleElement.Test
{
    [TestFixture]
    public class Tests
    {
        // test with null
        // test with empty list
        // test with list with one element
        // test with list with two elements.

        LinkedList<int> list = new LinkedList<int>();

        [SetUp]
        public void Setup()
        {
            list.AddFirst(1);
            list.AddLast(2);
            list.AddLast(3);
        }

        [Test]
        public void EraseMiddleWorksCorrectWithOddListLength()
        {
            var actual = EraseMiddle(list);
            var expected = new LinkedList<int>(new int[] { 1, 3 });

            Assert.That(actual, Has.Count.EqualTo(expected.Count));
            Assert.That(actual, Is.EqualTo(expected));
            Assert.That(actual, Does.Not.Contain(2));
        }

        [Test]
        public void EraseMiddleWithEvenListLengthRemovesElementAfterMiddle()
        {
            list.AddLast(4);
            var actual = EraseMiddle(list);
            var expected = new LinkedList<int>(new int[] { 1, 2, 4 });

            Assert.That(actual, Has.Count.EqualTo(expected.Count));
            Assert.That(actual, Is.EqualTo(expected));
        }
    }
}